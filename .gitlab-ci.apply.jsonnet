[{
  include: [
    { 'local': '.gitlab-ci/terraform-pipeline.yml' },
  ],
  stages: ['approve', 'apply', 'no-change'],
  variables: {
    P_PROJECT: std.extVar('P_PROJECT'),
    P_REGION: std.extVar('P_REGION'),
    P_ENVIRONMENT: std.extVar('P_ENVIRONMENT'),
    ENVIRONMENT: std.extVar('TF_WORKSPACE'),
  },
  [if std.extVar('CHANGES') != 0 then
    std.extVar('TF_WORKSPACE') + ':approve'
  ]: {
    extends: '.approve',
  },
  [if std.extVar('CHANGES') != 0 then
    std.extVar('TF_WORKSPACE') + ':apply']
  : {
    extends: '.apply',
    needs: [
      {
        job: std.extVar('TF_WORKSPACE') + ':approve',
        artifacts: false,
      },
      {
        project: std.extVar('CI_PROJECT_PATH'),
        ref: std.extVar('CI_COMMIT_REF_NAME'),
        job: std.extVar('TF_WORKSPACE') + ':validate',
        artifacts: true,
      }
      {
        project: std.extVar('CI_PROJECT_PATH'),
        ref: std.extVar('CI_COMMIT_REF_NAME'),
        job: std.extVar('TF_WORKSPACE') + ':plan',
        artifacts: true,
      },
    ],
  },
  [if std.extVar('CHANGES') == 0 then
    std.extVar('TF_WORKSPACE') + ':no-change']
  : {
    extends: '.no-change',
  },
}]
